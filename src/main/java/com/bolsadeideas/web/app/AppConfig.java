package com.bolsadeideas.web.app;

import com.bolsadeideas.web.app.models.domain.ItemFactura;
import com.bolsadeideas.web.app.models.domain.Producto;
import com.bolsadeideas.web.app.models.service.IServicio;
import com.bolsadeideas.web.app.models.service.MiServicio;
import com.bolsadeideas.web.app.models.service.MiServicioComplejo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.Arrays;
import java.util.List;

@Configuration
public class AppConfig {

    @Bean("miServicioSimple")
    public IServicio registrarMiServicio(){
        return new MiServicio();
    }


    @Bean("miServicioComplejo")
    @Primary
    public IServicio registrarMiServicioComplejo(){
        return new MiServicioComplejo();
    }

    @Bean("itemsFactura")
    public List<ItemFactura> registrarItems(){
        Producto producto = new Producto("Camara",100);
        Producto producto2 = new Producto("Bicicleta",102);
        Producto producto3 = new Producto("Laptop",10400);

        ItemFactura linea = new ItemFactura(producto,2);
        ItemFactura linea2 = new ItemFactura(producto2,3);
        ItemFactura linea3 = new ItemFactura(producto3,4);

        return Arrays.asList(linea,linea2,linea3);
    }
}
