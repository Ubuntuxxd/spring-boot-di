package com.bolsadeideas.web.app.controllers;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/params")
public class Params {

    @GetMapping("/param")
    public String params(HttpServletRequest request, Model model) {
        Integer numero;
        String texto = request.getParameter("texto");
        try {
            numero = Integer.valueOf(request.getParameter("numero"));
        } catch (NumberFormatException e){
            numero = 0;
        }
        model.addAttribute("resultado", "El texto completo es: " + texto + " . " + numero);
        model.addAttribute("titulo","Texto en parametros usando HttpServletRequest");
        return "params/ver";
    }
}
