package com.bolsadeideas.web.app.controllers;

import com.bolsadeideas.web.app.models.service.IServicio;
import com.bolsadeideas.web.app.models.service.MiServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    //@Qualifier("miServicioSimple")
    @Autowired
    private IServicio servicio;

    @GetMapping({"/", "/index", "/home"})
    public String index(Model model) {
        model.addAttribute("model", servicio.operacion());
        return "index";
    }
}
